USE [DB_PlasticWasteMgmt]
GO


CREATE OR ALTER PROCEDURE PlasticType_Insert
@PlasticType VARCHAR(50),
@Description VARCHAR(255),
@PriceperKg SMALLMONEY
AS
	BEGIN	
		INSERT INTO  PlasticType(PlasticType,Description,PriceperKg)
		VALUES(@PlasticType,@Description,@PriceperKg)
END;


EXEC PlasticType_Insert
@PlasticType ="PVC",
@Description ="Tyre,Tube elc",
@PriceperKg=60