USE [DB_PlasticWasteMgmt]
GO

CREATE OR ALTER PROCEDURE Admin_Update
@AdminId SMALLINT,
@AdminName varchar(50),
@Password varchar(255),
@EmailId varchar(100),
@ContactNo varchar(10)
AS
	BEGIN	
		UPDATE Admin SET AdminName=@AdminName,Password=@Password,EmailId=@EmailId,ContactNo=@ContactNo
		WHERE  AdminID=@AdminId;
END;

EXEC Admin_Update
@AdminId=1,
@AdminName='admin',
@Password='admin',
@EmailId='A@gmail.com',
@ContactNo='8574962200';