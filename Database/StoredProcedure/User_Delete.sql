USE [DB_PlasticWasteMgmt]
GO

CREATE OR ALTER PROCEDURE UserType_Delete
@TypeId TINYINT

AS
	BEGIN	
		DELETE 
		FROM UserType
		WHERE  UserTypeId=@TypeId;
END;

EXEC UserType_Delete
@TypeId=1;