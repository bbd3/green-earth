USE [DB_PlasticWasteMgmt]
GO

CREATE OR ALTER PROCEDURE SubType_Delete
@TypeId TINYINT

AS
	BEGIN	
		DELETE 
		FROM SubType
		WHERE  SubTypeId=@TypeId;
END;

EXEC SubType_Delete
@TypeId=1;