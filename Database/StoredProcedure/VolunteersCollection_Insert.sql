USE [DB_PlasticWasteMgmt]
GO

CREATE OR ALTER PROCEDURE VolunteersCollection_Insert
@UserId BIGINT ,
@DonateId BIGINT ,
@PlasticTypeId TINYINT ,
@TotalKg FlOAT,
@CollectionDate DATE
AS
	BEGIN	
		INSERT INTO VolunteersCollection(UserId,DonateId,PlasticTypeId,TotalKg,CollectionDate)
		VALUES(@UserId,@DonateId,@PlasticTypeId,@TotalKg,@CollectionDate)
END;


EXEC VolunteersCollection_Insert
@UserId=2,
@DonateId=2,
@PlasticTypeId=1,
@TotalKg=50,
@CollectionDate="10-10-2010"