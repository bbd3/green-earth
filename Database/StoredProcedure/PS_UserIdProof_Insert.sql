USE [DB_PlasticWasteMgmt]
GO

CREATE OR ALTER PROCEDURE UserIdProof_Insert
@UserId BIGINT,
@ProofId SMALLINT ,
@ProofImg VARBINARY(255)
AS
	BEGIN
		INSERT INTO UserIdProof(UserId,ProofId,ProofImg)
		VALUES(@UserId,@ProofId,@ProofImg)
END;

EXEC UserIdProof_Insert
@UserId =1,
@ProofId=1,
@ProofImg='kfhk';