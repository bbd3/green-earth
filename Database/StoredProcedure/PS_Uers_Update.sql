USE [DB_PlasticWasteMgmt]
GO

CREATE OR ALTER PROCEDURE Uers_Update
@UserId BIGINT,
@UserTypeId TINYINT,
@SubTypeId TINYINT,
@UserName VARCHAR(100),
@ContactNo VARCHAR(10),
@Email VARCHAR(100),
@Password VARCHAR(255)


AS
	BEGIN	
		UPDATE Users SET UserTypeId=@UserTypeId,SubTypeId=@SubTypeId,UserFullName=@UserName,ContactNo=@ContactNo,Email=@Email,Password=@Password
		WHERE UserId=@UserId
END;

EXEC Uers_Update
@UserId=1,
@UserTypeId=1,
@SubTypeId=1,
@UserName='kinjal',
@ContactNo='1245789636',
@Email='E@gmail.com',
@Password='shk';

