USE [DB_PlasticWasteMgmt]
GO

CREATE OR ALTER PROCEDURE ReceiverPurchase_Insert
@UserId BIGINT ,
@PlasticTypeId TINYINT,
@TotalKgPurchase Float,
@TotalPrice SMALLMONEY,
@PurchaseData DATE
AS
	BEGIN	
		INSERT INTO ReceiverPurchase(UserId,PlasticTypeId,TotalKgPurchase,TotalPrice,PurchaseData)
		VALUES(@UserId,@PlasticTypeId,@TotalKgPurchase,@TotalPrice,@PurchaseData)
END;


EXEC ReceiverPurchase_Insert
@UserId =1 ,
@PlasticTypeId =1,
@TotalKgPurchase =50,
@TotalPrice=500,
@PurchaseData ="12-12-2010"