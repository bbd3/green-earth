USE [DB_PlasticWasteMgmt]
GO

CREATE OR ALTER PROCEDURE VolunteersCollection_Update
@CollectionId BIGINT,
@UserId BIGINT ,
@DonateId BIGINT ,
@PlasticTypeId TINYINT ,
@TotalKg FlOAT,
@CollectionDate DATE
AS
	BEGIN	
		UPDATE VolunteersCollection SET UserId=@UserId,DonateId=@DonateId,PlasticTypeId=@PlasticTypeId,TotalKg=@TotalKg,CollectionDate=@CollectionDate
		WHERE  CollectionId=@CollectionId;
		
END;

EXEC VolunteersCollection_Update
@CollectionId=1,
@UserId=2,
@DonateId=2,
@PlasticTypeId=1,
@TotalKg=50,
@CollectionDate="10-10-2010"
