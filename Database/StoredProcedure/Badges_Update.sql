USE [DB_PlasticWasteMgmt]
GO


CREATE OR ALTER PROCEDURE Badges_Update
@BadgeId TINYINT,
@BadgeName VARCHAR(50),
@UserTypeId TINYINT,
@SubTypeId TINYINT,
@BadgeCondition FLOAT
AS
	BEGIN	
		UPDATE Badges SET BadgeName=@BadgeName,UserTypeId=@UserTypeId,SubTypeId=@SubTypeId,BadgeCondition=@BadgeCondition
		WHERE  BadgeId=@BadgeId;

END;


EXEC Badges_Update
@BadgeId=1,
@BadgeName="Silver",
@UserTypeId=1,
@SubTypeId=1,
@BadgeCondition=100.5


