USE [DB_PlasticWasteMgmt]
GO

CREATE OR ALTER PROCEDURE PlasticType_Delete
@PlasticTypeId TINYINT

AS
	BEGIN	
		DELETE 
		FROM PlasticType
		WHERE  PlasticTypeId=@PlasticTypeId;
END;

EXEC PlasticType_Delete
@PlasticTypeId=1;