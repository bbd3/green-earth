USE [DB_PlasticWasteMgmt]
GO

CREATE OR ALTER PROCEDURE SubType_Update
@TypeId TINYINT,
@Type VARCHAR(50)

AS
	BEGIN	
		UPDATE SubType SET  SubType=@Type
		WHERE  SubTypeId=@TypeId;
END;

EXEC SubType_Update
@TypeId=1,
@Type='NGO';
