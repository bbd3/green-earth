USE [DB_PlasticWasteMgmt]
GO

CREATE OR ALTER PROCEDURE Badges_Delete
@BadgeId TINYINT

AS
	BEGIN	
		DELETE 
		FROM Badges
		WHERE  BadgeId=@BadgeId;
END;

EXEC Badges_Delete
@BadgeId=1;