USE [DB_PlasticWasteMgmt]
GO

CREATE OR ALTER PROCEDURE Address_Insert
@AddressLine1 VARCHAR(100),
@AddressLine2 VARCHAR(100),
@City VARCHAR(100),
@Pincode VARCHAR(6),
@UserId BIGINT

AS
	BEGIN	
		INSERT INTO Adress(AddressLine1,AddressLine2,City,Pincode,UserId)
		VALUES(@AddressLine1,@AddressLine2,@City,@Pincode,@UserId)
END;

EXEC Address_Insert
@AddressLine1 'kdjfh',
@AddressLine2 'surat',
@City 'surat',
@Pincode '395005',
@UserId 1;


