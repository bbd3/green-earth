USE [DB_PlasticWasteMgmt]
GO


CREATE OR ALTER PROCEDURE UserAchievements_Update
@AchievementId INT,
@BadgeId TINYINT,
@UserId BIGINT,
@AchievementDate DATE
AS
	BEGIN	
		UPDATE UserAchievements SET BadgeId=@BadgeId,UserId=@UserId,AchievementDate=@AchievementDate
		WHERE  AchievementId=@AchievementId;
END;


EXEC  UserAchievements_Update
@AchievementId=1,
@BadgeId=2,
@UserId=1,
@AchievementDate= "12-12-2020"
