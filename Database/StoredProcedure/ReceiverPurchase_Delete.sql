USE [DB_PlasticWasteMgmt]
GO

CREATE OR ALTER PROCEDURE ReceiverPurchase_Delete
@PurchaseId TINYINT

AS
	BEGIN	
		DELETE 
		FROM ReceiverPurchase
		WHERE PurchaseId=@PurchaseId;
END;

EXEC ReceiverPurchase_Delete
@PurchaseId=1