USE [DB_PlasticWasteMgmt]
GO

CREATE OR ALTER PROCEDURE PlasticDonate_Delete
@DonateId SMALLINT

AS
	BEGIN	
		DELETE 
		FROM PlasticDonate
		WHERE  DonateId=@DonateId;
END;

EXEC PlasticDonate_Delete
@DonateId=1;