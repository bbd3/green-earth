USE [DB_PlasticWasteMgmt]
GO


CREATE OR ALTER PROCEDURE PlasticDonate_Insert
@UserId BIGINT,
@CurrentAddress Geography,
@TotalKg FLOAT,
@DonateDate DATE
AS
	BEGIN	
		INSERT INTO PlasticDonate(UserId,CurrentAddress,TotalKg,DonateDate)
		VALUES(@UserId,@CurrentAddress,@TotalKg,@DonateDate)
END;

DECLARE @geographyPoint geography = geography::Point('47.65100', '-122.34900', '4326');

EXEC PlasticDonate_Insert
@UserId=1,
@CurrentAddress=@geographyPoint,
@TotalKg =50.5,
@DonateDate= "12-12-2020"