USE [DB_PlasticWasteMgmt]
GO

CREATE OR ALTER PROCEDURE Address_Update
@AddressId INT,
@AddressLine1 VARCHAR(100),
@AddressLine2 VARCHAR(100),
@City VARCHAR(100),
@Pincode VARCHAR(6),
@UserId BIGINT

AS
	BEGIN	
		UPDATE Adress SET AddressLine1=@AddressLine1,AddressLine2=@AddressLine2,City=@City,Pincode=@Pincode,UserId=@UserId
		WHERE AddressId=@AddressId
END;

EXEC Address_Update
@AddressId=1,
@AddressLine1 ='jjejf',
@AddressLine2= 'surat',
@City ='surat',
@Pincode ='395005',
@UserId= 1;


