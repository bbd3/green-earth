USE [DB_PlasticWasteMgmt]
GO

CREATE OR ALTER PROCEDURE Admin_Delete
@AdminId SMALLINT

AS
	BEGIN	
		DELETE 
		FROM Admin
		WHERE  AdminID=@AdminId;
END;

EXEC Admin_Delete
@AdminId=2;