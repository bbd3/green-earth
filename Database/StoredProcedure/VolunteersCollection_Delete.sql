USE [DB_PlasticWasteMgmt]
GO

CREATE OR ALTER PROCEDURE VolunteersCollection_Delete
@CollectionId TINYINT

AS
	BEGIN	
		DELETE 
		FROM VolunteersCollection
		WHERE CollectionId=@CollectionId;
END;

EXEC VolunteersCollection_Delete
@CollectionId=1