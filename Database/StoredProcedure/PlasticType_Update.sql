USE [DB_PlasticWasteMgmt]
GO

CREATE OR ALTER PROCEDURE PlasticType_Update
@PlasticTypeId TINYINT,
@PlasticType VARCHAR(50),
@Description VARCHAR(255),
@PriceperKg SMALLMONEY
AS
	BEGIN	
		UPDATE PlasticType SET PlasticType=@PlasticType,Description=@Description,PriceperKg=@PriceperKg
		WHERE  PlasticTypeId=@PlasticTypeId;
		
END;


EXEC PlasticType_Update
@PlasticTypeId=1,
@PlasticType ="PVC",
@Description ="Tyre,Tube elc",
@PriceperKg=60
