USE [DB_PlasticWasteMgmt]
GO

CREATE OR ALTER PROCEDURE PlasticStock_Insert
@PlasticTypeId TINYINT ,
@TotalKg FLOAT
AS
	BEGIN	
		INSERT INTO PlasticStock(PlasticTypeId,TotalKg)
		VALUES(@PlasticTypeId,@TotalKg)
END;


EXEC PlasticStock_Insert
@PlasticTypeId=1,
@TotalKg=50
