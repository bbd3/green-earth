USE [DB_PlasticWasteMgmt]
GO

CREATE OR ALTER PROCEDURE PlasticStock_Update
@StockId TINYINT,
@PlasticTypeId TINYINT ,
@TotalKg FLOAT
AS
	BEGIN	
		UPDATE PlasticStock SET PlasticTypeId=@PlasticTypeId,TotalKg=@TotalKg
		WHERE  StockId=@StockId;
		
END;

EXEC PlasticStock_Update
@StockId=1,
@PlasticTypeId=1,
@TotalKg=50