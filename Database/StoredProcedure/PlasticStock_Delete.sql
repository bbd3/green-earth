USE [DB_PlasticWasteMgmt]
GO

CREATE OR ALTER PROCEDURE PlasticStock_Delete
@StockId TINYINT

AS
	BEGIN	
		DELETE 
		FROM PlasticStock
		WHERE StockId=@StockId;
END;

EXEC PlasticStock_Delete
@StockId=1