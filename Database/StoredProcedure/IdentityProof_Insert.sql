USE [DB_PlasticWasteMgmt]
GO

CREATE OR ALTER PROCEDURE IdentityProof_Insert
@ProofName VARCHAR(50),
@SubTypeId TINYINT
AS
	BEGIN
		INSERT INTO IdentityProof(ProofName,SubTypeId)
		VALUES(@ProofName,@SubTypeId)
END;

EXEC IdentityProof_Insert
@ProofName='Adhar Card',
@SubTypeId=1;