USE [DB_PlasticWasteMgmt]
GO

CREATE OR ALTER PROCEDURE UserType_Update
@TypeId TINYINT,
@Type VARCHAR(50)

AS
	BEGIN	
		UPDATE UserType SET UserType=@Type
		WHERE  UserTypeId=@TypeId;
END;

EXEC UserType_Update
@TypeId=1,
@Type='Reciever';
