USE [DB_PlasticWasteMgmt]
GO


CREATE OR ALTER PROCEDURE Badges_Insert
@BadgeName VARCHAR(50),
@UserTypeId TINYINT,
@SubTypeId TINYINT,
@BadgeCondition FLOAT
AS
	BEGIN	
		INSERT INTO Badges(BadgeName,UserTypeId,SubTypeId,BadgeCondition)
		VALUES(@BadgeName,@UserTypeId,@SubTypeId,@BadgeCondition)
END;



EXEC Badges_Insert
@BadgeName="Golden",
@UserTypeId=1,
@SubTypeId=1,
@BadgeCondition=100.5
