USE [DB_PlasticWasteMgmt]
GO

CREATE OR ALTER PROCEDURE PlasticDonate_Update
@DonateId BIGINT,
@UserId BIGINT,
@CurrentAddress Geography,
@TotalKg FLOAT,
@DonateDate DATE
AS
	BEGIN	
		UPDATE PlasticDonate SET UserId=@UserId,CurrentAddress=@CurrentAddress,TotalKg=@TotalKg,DonateDate=@DonateDate
		WHERE  DonateId=@DonateId;
END;

DECLARE @geographyPoint geography = geography::Point('47.65100', '-122.34900', '4326');

EXEC PlasticDonate_Update
@DonateId=2,
@UserId=4,
@CurrentAddress=@geographyPoint,
@TotalKg =40.5,
@DonateDate= "12-1-2021"