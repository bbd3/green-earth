USE [DB_PlasticWasteMgmt]
GO

CREATE OR ALTER PROCEDURE ReceiverPurchase_Update
@PurchaseId BIGINT,
@UserId BIGINT ,
@PlasticTypeId TINYINT,
@TotalKgPurchase Float,
@TotalPrice SMALLMONEY,
@PurchaseData DATE
AS
	BEGIN	
			UPDATE ReceiverPurchase SET UserId=@UserId,PlasticTypeId=@PlasticTypeId,TotalKgPurchase=@TotalKgPurchase,TotalPrice=@TotalPrice
		WHERE  PurchaseId=@PurchaseId;
		
END;


EXEC ReceiverPurchase_Update
@PurchaseId=1,
@UserId =1 ,
@PlasticTypeId =1,
@TotalKgPurchase =50,
@TotalPrice=500,
@PurchaseData ="12-12-2010"