
--Function for validating Users(PlasticProvider,PlasticReceiver) for Login Purpose

USE [DB_PlasticWasteMgmt]
GO

CREATE OR ALTER FUNCTION Isvalid_User
(@username VARCHAR(50),@Password VARCHAR(255))
RETURNS INT
AS 
BEGIN
	 DECLARE @res INT
	SELECT @res=COUNT(*)
			FROM PlasticProvider
			WHERE (EmailId=@username OR ContactNo=@username ) AND 
					(Password=@Password);

	RETURN @res
END


SELECT DBO.Isvalid_user('8574962258','4jkhd5488458n');

--Function to convert ton to kg
CREATE OR ALTER FUNCTION TonneToKg(
@Tonne float(53))
RETURNS FLOAT(53)
AS
BEGIN
    DECLARE @kg FLOAT(53)=@Tonne*1000;
    RETURN @kg
END

SELECT dbo.TonneToKg(1.5);
