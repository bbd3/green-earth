USE [DB_PlasticWasteMgmt]
GO

CREATE OR ALTER PROCEDURE Admin_Insert
@AdminName varchar(50),
@Password varchar(255),
@EmailId varchar(100),
@ContactNo varchar(10)
AS
	BEGIN	
		INSERT INTO Admin(AdminName,Password,EmailId,ContactNo)
		VALUES(@AdminName,@Password,@EmailId,@ContactNo)
END;

EXEC Admin_Insert
@AdminName='admin',
@Password='admin',
@EmailId='Admin@gmail.com',
@ContactNo='8574962255';


