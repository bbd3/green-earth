USE [DB_PlasticWasteMgmt]
GO

CREATE OR ALTER PROCEDURE UserType_Insert
@Type varchar(50)

AS
	BEGIN	
		INSERT INTO UserType(UserType)
		VALUES(@Type)
END;

EXEC UserType_Insert
@Type='Provider';



