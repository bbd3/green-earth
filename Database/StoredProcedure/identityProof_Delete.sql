USE [DB_PlasticWasteMgmt]
GO

CREATE OR ALTER PROCEDURE IdentityProof_Delete
@ProofId SMALLINT

AS
	BEGIN
		DELETE 
		FROM IdentityProof 
		WHERE ProofId=@ProofId
END;

EXEC IdentityProof_Delete
@ProofId =1;
