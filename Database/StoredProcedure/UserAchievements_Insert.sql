USE [DB_PlasticWasteMgmt]
GO


CREATE OR ALTER PROCEDURE UserAchievements_Insert
@BadgeId TINYINT,
@UserId BIGINT,
@AchievementDate DATE
AS
	BEGIN	
		INSERT INTO  UserAchievements(BadgeId,UserId,AchievementDate)
		VALUES(@BadgeId,@UserId,@AchievementDate)
END;


EXEC  UserAchievements_Insert
@BadgeId=2,
@UserId=1,
@AchievementDate= "12-12-2020"