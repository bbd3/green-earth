USE [DB_PlasticWasteMgmt]
GO

CREATE OR ALTER PROCEDURE UserIdProof_Delete
@UserProofId BIGINT

AS
	BEGIN
		DELETE 
		FROM UserIdProof
		WHERE UserProofId=@UserProofId
END;

EXEC UserIdProof_Delete
@UserProofId=1;
