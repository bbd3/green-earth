USE [DB_PlasticWasteMgmt]
GO

CREATE OR ALTER PROCEDURE IdentityProof_Update
@ProofId SMALLINT,
@ProofName VARCHAR(50),
@SubTypeId TINYINT
AS
	BEGIN
		UPDATE IdentityProof SET ProofName=@ProofName,SubTypeId=@SubTypeId
		WHERE ProofId=@ProofId
END;

EXEC IdentityProof_Update
@ProofId =1,
@ProofName='Adhar Card No',
@SubTypeId=1;