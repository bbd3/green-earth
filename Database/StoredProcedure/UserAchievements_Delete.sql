USE [DB_PlasticWasteMgmt]
GO

CREATE OR ALTER PROCEDURE UserAchievements_Delete
@AchievementId TINYINT

AS
	BEGIN	
		DELETE 
		FROM UserAchievements
		WHERE  AchievementId=@AchievementId;
END;

EXEC UserAchievements_Delete
@AchievementId=1;