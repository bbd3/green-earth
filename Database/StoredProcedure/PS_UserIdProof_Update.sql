USE [DB_PlasticWasteMgmt]
GO

CREATE OR ALTER PROCEDURE UserIdProof_Update
@UserProofId BIGINT,
@UserId BIGINT,
@ProofId SMALLINT ,
@ProofImg VARBINARY(255)
AS
	BEGIN
		UPDATE UserIdProof SET UserId=@UserId,ProofId=@ProofId,ProofImg=@ProofImg
		WHERE UserProofId=@UserProofId
END;

EXEC UserIdProof_Update
@UserProofId=1,
@UserId =1,
@ProofId=1,
@ProofImg='kfhk';