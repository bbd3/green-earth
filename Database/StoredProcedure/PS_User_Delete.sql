USE [DB_PlasticWasteMgmt]
GO

CREATE OR ALTER PROCEDURE User_Delete
@UserId TINYINT

AS
	BEGIN	
		DELETE 
		FROM Users
		WHERE  UserId=@UserId;
END;

EXEC User_Delete
@UserId=1;