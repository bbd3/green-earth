
USE DB_PlasticWasteMgmt
GO

CREATE OR ALTER TRIGGER AfterInsertPlasticDonate
ON volunteers_collection
AFTER INSERT
AS

DECLARE @plastic_type_Id tinyint
DECLARE @plastic_stock FLOAT
DECLARE @current_stock FLOAT

SELECT @plastic_type_Id=INSERTED.plastic_type_id
FROM inserted; 

SELECT @plastic_stock=INSERTED.total_kg
FROM inserted;

if exists (SELECT   @plastic_type_Id 
			FROM  plastic_stock)
	BEGIN
		
		SELECT @current_stock=total_kg
		FROM plastic_stock
		WHERE plastic_type_id=@plastic_type_Id;
		
		UPDATE plastic_stock SET total_kg=(@plastic_stock+@current_stock)
		WHERE plastic_type_id=@plastic_type_Id;
		
		
	END
else
	BEGIN
	INSERT INTO plastic_stock(plastic_type_id,total_kg) VALUES(@plastic_type_Id,@plastic_stock)
		
	END
		


GO