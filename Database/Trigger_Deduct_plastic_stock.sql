
USE DB_PlasticWasteMgmt
GO

CREATE OR ALTER TRIGGER AfterInsertReceiverPurchase
ON receiver_purchase
AFTER INSERT
AS

DECLARE @plastic_type_Id tinyint
DECLARE @plastic_stock FLOAT
DECLARE @current_stock FLOAT

SELECT @plastic_type_Id=INSERTED.plastic_type_id
FROM inserted; 
	
SELECT @plastic_stock=INSERTED.total_kg_purchase
FROM inserted; 		
		
		UPDATE plastic_stock SET total_kg=(total_kg-@plastic_stock)
		WHERE plastic_type_id=@plastic_type_Id;
		
GO