use master
go
DROP DATABASE IF EXISTS DB_PlasticWasteMgmt
GO
CREATE DATABASE DB_PlasticWasteMgmt;
GO

USE DB_PlasticWasteMgmt
GO

-- Create admin table
CREATE TABLE admin
(
admin_id SMALLINT PRIMARY KEY IDENTITY(1,1),
admin_name VARCHAR(50),
password VARCHAR(255),
email_id VARCHAR(100),
contact_no VARCHAR(10)
);

ALTER TABLE admin
ADD CONSTRAINT UniqueAdminFields UNIQUE(email_id,contact_no);

--Create UserType
CREATE TABLE user_type
(
user_type_id TINYINT PRIMARY KEY IDENTITY(1,1),
user_type VARCHAR(50)
);

--Create tabel SubType
CREATE TABLE sub_type
(
sub_type_id TINYINT PRIMARY KEY IDENTITY(1,1),
sub_type VARCHAR(50)
);

--Create Address Table
CREATE TABLE address
(
address_id INT PRIMARY KEY IDENTITY(1,1),
address_line1 VARCHAR(100),
address_line2 VARCHAR(100),
city VARCHAR(100),
pincode VARCHAR(6)
);


--Create table User
CREATE TABLE users
(
user_id BIGINT PRIMARY KEY IDENTITY(1,1),
user_type_id TINYINT REFERENCES user_type(user_type_id),
sub_type_id TINYINT REFERENCES sub_type(sub_type_id),
user_full_name VARCHAR(255),
contact_no VARCHAR(10) UNIQUE,
email_id VARCHAR(100) UNIQUE,
password VARCHAR(255),
address_id INT REFERENCES address(address_id)
);


--Create IdentityProof
CREATE TABLE identity_proof
(
proof_id SMALLINT PRIMARY KEY IDENTITY(1,1),
proof_name VARCHAR(50),
sub_type_id TINYINT REFERENCES sub_type(sub_type_id)
);


--Create ProviderIdproof TABLE

CREATE TABLE user_id_proof(
user_proof_id BIGINT PRIMARY KEY IDENTITY(1,1),
user_id BIGINT REFERENCES users(user_id),
proof_id SMALLINT REFERENCES  identity_proof(proof_id),
proof_img VARBINARY(255)
);


--Create plastic donation table
CREATE TABLE plastic_donate
(
donate_id BIGINT PRIMARY KEY IDENTITY(1,1),
user_id BIGINT REFERENCES users(user_id),
current_address Geography,
current_address_text as current_address.STAsText(),
total_kg FLOAT(53),
donate_date DATE
);

--Create Badges table
CREATE TABLE badges
(
badge_id TINYINT PRIMARY KEY IDENTITY(1,1),
badge_name VARCHAR(50)  UNIQUE,
user_type_id TINYINT REFERENCES user_type(user_type_id),
sub_type_id TINYINT REFERENCES sub_type(sub_type_id),
badge_condition FLOAT(53)
);

--Create table User Achievements 
CREATE TABLE user_achievements
(
achievement_id INT PRIMARY KEY IDENTITY(1,1),
badge_id TINYINT REFERENCES badges(badge_id),
user_id BIGINT REFERENCES users(user_id),
achievement_date DATE
);


--Create Plastic Type
CREATE TABLE plastic_type
(
plastic_type_id TINYINT PRIMARY KEY IDENTITY(1,1),
plastic_type VARCHAR(50),
description VARCHAR(255),
price_per_kg SMALLMONEY
);

--Create volunteers collction table
CREATE TABLE volunteers_collection
(
collection_id BIGINT PRIMARY KEY IDENTITY(1,1),
user_id BIGINT REFERENCES users(user_id),
donate_id BIGINT REFERENCES plastic_donate(donate_id),
plastic_type_id TINYINT REFERENCES plastic_type(plastic_type_id),
total_kg FlOAT(53),
collection_date DATE
);

--Create PlasticStock
CREATE TABLE plastic_stock
(
stock_id TINYINT PRIMARY KEY IDENTITY(1,1),
plastic_type_id TINYINT REFERENCES plastic_type(plastic_type_id),
total_kg FLOAT(53)
);


--Create Receiver purchase table
CREATE TABLE receiver_purchase
(
purchase_id BIGINT PRIMARY KEY IDENTITY(1,1),
user_id BIGINT REFERENCES users(user_id),
plastic_type_id TINYINT REFERENCES  plastic_type(plastic_type_id),
total_kg_purchase Float(53),
total_price SMALLMONEY,
purchase_date DATE
);
