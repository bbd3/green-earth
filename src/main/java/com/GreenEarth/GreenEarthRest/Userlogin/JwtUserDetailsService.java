package com.GreenEarth.GreenEarthRest.Userlogin;

import com.GreenEarth.GreenEarthRest.model.Admin;
import com.GreenEarth.GreenEarthRest.model.User;
import com.GreenEarth.GreenEarthRest.repo.UserRepo;
import com.GreenEarth.GreenEarthRest.service.GrrenEarthUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class JwtUserDetailsService implements UserDetailsService {

    @Autowired
    private UserRepository userRepo;

    @Autowired
    private AdminRepository adminRepository;

    @Autowired
    private PasswordEncoder bcryptEncoder;

    @Autowired
    private GrrenEarthUserService  grrenEarthUserService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException
    {

        User user = userRepo.findByEmailid(username);

        if (user != null)
        {


            return new org.springframework.security.core.userdetails.User(user.getEmailid(), user.getPassword(),
                    new ArrayList<>());
        }

        Admin admin=adminRepository.findByEmailid(username);
        if(admin != null)
        {

            return new org.springframework.security.core.userdetails.User(admin.getEmailid(), admin.getPassword(),
                    new ArrayList<>());
        }
        else
        {
            throw new UsernameNotFoundException("No user Found");
        }


//        return new org.springframework.security.core.userdetails.User("admin", "admin",
//                new ArrayList<>());
    }

    public User save(User user) {
//        User newUser = new User();
//        newUser.setUserType(user.getUserType());
//        newUser.setSubType(user.getSubType());
//        newUser.setUser_full_name(user.getUser_full_name());
//        newUser.setContact_no(user.getContact_no());
//        newUser.setEmailid(user.getEmailid());
//        newUser.setPassword(bcryptEncoder.encode(user.getPassword()));
//        newUser.setAddress(user.getAddress());
        user.setPassword(bcryptEncoder.encode(user.getPassword()));
       return grrenEarthUserService.addUser(user);
//        return userRepo.save(newUser);
    }

    public Admin saveAdmin(Admin admin)
    {
        Admin newAdmin=new Admin();
        newAdmin.setAdmin_name(admin.getAdmin_name());
        newAdmin.setPassword(bcryptEncoder.encode(admin.getPassword()));
        newAdmin.setEmailid(admin.getEmailid());
        newAdmin.setContact_no(admin.getContact_no());
        return  adminRepository.save(newAdmin);
    }
}
