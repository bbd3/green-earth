package com.GreenEarth.GreenEarthRest.Userlogin;

import com.GreenEarth.GreenEarthRest.model.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, Integer> {
    User findByEmailid(String username);
}
