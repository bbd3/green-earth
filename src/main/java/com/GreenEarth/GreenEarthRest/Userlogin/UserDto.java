package com.GreenEarth.GreenEarthRest.Userlogin;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserDto {
    private String username;
    private String password;
}
