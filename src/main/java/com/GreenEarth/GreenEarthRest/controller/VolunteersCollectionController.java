package com.GreenEarth.GreenEarthRest.controller;

import com.GreenEarth.GreenEarthRest.Exception.ResourceNotFoundException;
import com.GreenEarth.GreenEarthRest.model.*;
import com.GreenEarth.GreenEarthRest.repo.PlasticStockRepo;
import com.GreenEarth.GreenEarthRest.repo.PlasticTypeRepo;
import com.GreenEarth.GreenEarthRest.repo.VolunteersCollectionRepo;
import com.GreenEarth.GreenEarthRest.service.VolunteersCollections;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class VolunteersCollectionController
{
    @Autowired
    private VolunteersCollections volunteersCollectionsService;
@Autowired
private  PlasticStockRepo plasticStockRepo;

    public  PlasticStockController plasticStockController =new PlasticStockController( plasticStockRepo) ;

    public VolunteersCollectionController() {
    }

    @PostMapping("/volunteers-collections")
    public ResponseEntity<VolunteersCollection> insertVolunteersCollection(@RequestBody VolunteersCollection volunteersCollection){
        volunteersCollectionsService.addVolunteersCollection(volunteersCollection);
        return new ResponseEntity<>(volunteersCollection, HttpStatus.CREATED);
    }

    @GetMapping("/volunteers-collections")
    public ResponseEntity<List<VolunteersCollection>> getAllVolunteersCollection(){
        List<VolunteersCollection> volunteersCollections =volunteersCollectionsService.getAllVolunteersCollection();

        if(volunteersCollections.isEmpty())
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        return new ResponseEntity<>(volunteersCollections,HttpStatus.OK);

    }
    @GetMapping("/volunteers-collections/{VolunteersCollectionId}")
    public ResponseEntity<VolunteersCollection> getVolunteersCollectionById(@PathVariable("VolunteersCollectionId") Integer VolunteersCollectionId){
        VolunteersCollection volunteersCollection=volunteersCollectionsService.getVolunteersCollectionById(VolunteersCollectionId);

        return new ResponseEntity<>(volunteersCollection,HttpStatus.OK);

    }
        @GetMapping("/volunteers-collections/userId/{userId}")
    public ResponseEntity<List<VolunteersCollection>> getUserAchievementsByUserId(@PathVariable Integer userId){
        List<VolunteersCollection> volunteersCollections1 =volunteersCollectionsService.getVolunteersCollectionByUserID(userId);


        if(volunteersCollections1.isEmpty())
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        return new ResponseEntity<>(volunteersCollections1,HttpStatus.OK);
    }


    @DeleteMapping("/volunteers-collections/{VolunteersCollectionId}")
    public ResponseEntity<HttpStatus> deleteVolunteersCollectionById(@PathVariable("VolunteersCollectionId") Integer VolunteersCollectionId){
        volunteersCollectionsService.deleteVolunteersCollection(VolunteersCollectionId);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
    @PutMapping("/volunteers-collections/{VolunteersCollectionId}")
    public ResponseEntity<Object> updateVolunteersCollectionById(@RequestBody VolunteersCollection volunteersCollection, @PathVariable("VolunteersCollectionId") Integer VolunteersCollectionId){
        VolunteersCollection volunteersCollection1=volunteersCollectionsService.getVolunteersCollectionById(VolunteersCollectionId);


        volunteersCollection1.setUser(volunteersCollection.getUser());

        volunteersCollection1.setPlasticType(volunteersCollection1.getPlasticType());
        volunteersCollection1.setTotal_kg(volunteersCollection1.getTotal_kg());
        volunteersCollection1.setCollection_date(volunteersCollection.getCollection_date());
        volunteersCollectionsService.editVolunteersCollection(volunteersCollection1);
        return new ResponseEntity<>(volunteersCollection1,HttpStatus.OK);
    }


}
