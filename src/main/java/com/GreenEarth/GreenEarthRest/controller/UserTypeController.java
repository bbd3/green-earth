package com.GreenEarth.GreenEarthRest.controller;

import com.GreenEarth.GreenEarthRest.Exception.ResourceNotFoundException;
import com.GreenEarth.GreenEarthRest.model.PlasticType;
import com.GreenEarth.GreenEarthRest.model.SubType;
import com.GreenEarth.GreenEarthRest.model.UserType;
import com.GreenEarth.GreenEarthRest.repo.UserTypeRepo;
import com.GreenEarth.GreenEarthRest.service.UserTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class UserTypeController
{
    @Autowired
    private UserTypeService userTypeService;

    public UserTypeController(UserTypeService userTypeService) {
        this.userTypeService = userTypeService;
    }

    @PostMapping("/user-types")
    public ResponseEntity<UserType> insertUserType(@RequestBody UserType userType){
        userTypeService.addUserType(userType);
        return new ResponseEntity<>(userType, HttpStatus.CREATED);
    }

    @GetMapping("/user-types")
    public ResponseEntity<List<UserType>> getAllUserTypes()
    {
        List<UserType> userTypes =userTypeService.getAllUserType();


        if(userTypes.isEmpty())
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        return new ResponseEntity<>(userTypes,HttpStatus.OK);
    }
    @GetMapping("/user-types/{UserId}")
    public ResponseEntity<UserType> getUserTypeById(@PathVariable("UserId") Integer UserId)
    {
        UserType userType=userTypeService.getUserTypeById(UserId);


        return new ResponseEntity<>(userType,HttpStatus.OK);

    }
    @DeleteMapping("/user-types/{UserId}")
    public ResponseEntity<HttpStatus> deleteUserTypeById(@PathVariable Integer UserId){
        userTypeService.deleteUserType(UserId);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
    @PutMapping("/user-types/{UserId}")
    public ResponseEntity<Object> updateUserTypeById(@RequestBody UserType userType, @PathVariable("UserId") Integer UserId)
    {
        UserType userType1= userTypeService.getUserTypeById(UserId);

        userType1.setUser_type(userType.getUser_type());

        userTypeService.editUserType(userType1);

        return new ResponseEntity<>(userType1,HttpStatus.OK);
    }
}
