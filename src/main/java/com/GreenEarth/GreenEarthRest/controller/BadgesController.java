package com.GreenEarth.GreenEarthRest.controller;

import com.GreenEarth.GreenEarthRest.Exception.ResourceNotFoundException;
import com.GreenEarth.GreenEarthRest.model.Address;
import com.GreenEarth.GreenEarthRest.model.Badges;
import com.GreenEarth.GreenEarthRest.model.User;
import com.GreenEarth.GreenEarthRest.repo.BadgesRepo;
import com.GreenEarth.GreenEarthRest.repo.UserRepo;
import com.GreenEarth.GreenEarthRest.service.badgesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class BadgesController {

    @Autowired
    private badgesService  badgesService;



    @PostMapping("/badges")
    public ResponseEntity<Badges> insertBadge(@RequestBody Badges badge){
       badgesService.addBadge(badge);
        return new ResponseEntity<>(badge, HttpStatus.CREATED);
    }


    @GetMapping("/badges")
    public ResponseEntity<List<Badges>> getAllBadges()
    {
        List<Badges> badges =badgesService.getAllBadges();

        if(badges.isEmpty())
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        return new ResponseEntity<>(badges,HttpStatus.OK);
    }
    @GetMapping("/badges/{BadgeId}")
    public ResponseEntity<Badges> getBadgeById(@PathVariable("BadgeId") Integer BadgeId)
    {
        Badges badge=badgesService.getBadgesIdById(BadgeId);


        return new ResponseEntity<>(badge,HttpStatus.OK);

    }
    @DeleteMapping("/badges/{BadgeId}")
    public ResponseEntity<HttpStatus> deleteBadgeById(@PathVariable Integer BadgeId){
        badgesService.deleteBadge(BadgeId);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
    @PutMapping("/badges/{BadgeId}")
    public ResponseEntity<Object> updateBadgeById(@RequestBody Badges badge, @PathVariable("BadgeId") Integer BadgeId)
    {
        Badges badges= badgesService.getBadgesIdById(BadgeId);

        badges.setBadge_name(badge.getBadge_name());
        badges.setBadge_condition(badge.getBadge_condition());
        badgesService.editBadge(badges);
        return new ResponseEntity<>(badges,HttpStatus.OK);
    }

}
