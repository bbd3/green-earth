package com.GreenEarth.GreenEarthRest.controller;

import com.GreenEarth.GreenEarthRest.Exception.ResourceNotFoundException;
import com.GreenEarth.GreenEarthRest.model.Admin;
import com.GreenEarth.GreenEarthRest.model.PlasticStock;
import com.GreenEarth.GreenEarthRest.repo.PlasticStockRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
public class PlasticStockController
{
    @Autowired
    private PlasticStockRepo repo;

    public PlasticStockController(PlasticStockRepo repo) {
        this.repo = repo;
    }

    @PostMapping("/plasticstock")
    public ResponseEntity<PlasticStock> addPlasticStock(@RequestBody PlasticStock plasticStock)
    {
        repo.save(plasticStock);
        return new ResponseEntity<>(plasticStock, HttpStatus.CREATED);
    }

    @GetMapping("/plasticstocks")
    public ResponseEntity<List<PlasticStock>> getAllPlasticStock()
    {
        List<PlasticStock> plasticStocks =new ArrayList<PlasticStock>();
        plasticStocks=repo.findAll();

        if(plasticStocks.isEmpty())
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        return new ResponseEntity<>(plasticStocks,HttpStatus.OK);
    }

    @GetMapping("/plasticstock/{PlasticStockId}")
    public ResponseEntity<PlasticStock> getPlasticStockById(@PathVariable Integer PlasticStockId)
    {
        PlasticStock plasticStock=repo.findById(PlasticStockId)
                .orElseThrow(()->new ResourceNotFoundException("Not found Tutorial with id = " + PlasticStockId));

        return new ResponseEntity<>(plasticStock,HttpStatus.OK);
    }

    @DeleteMapping("/plasticstock/{PlasticStockId}")
    public ResponseEntity<HttpStatus> deletePlasticStockById(@PathVariable Integer PlasticStockId){
        repo.deleteById(PlasticStockId);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }


    @PutMapping("/plasticstock/{PlasticStockId}")
    public ResponseEntity<Object> updatePlasticStockById(@RequestBody PlasticStock plasticStock, @PathVariable("PlasticStockId") Integer PlasticStockId)
    {
        PlasticStock plasticStock1=repo.findById(PlasticStockId)
                .orElseThrow(()-> new ResourceNotFoundException("Not found Admin with id = "+PlasticStockId ));

        plasticStock1.setPlasticType(plasticStock.getPlasticType());
        plasticStock1.setTotal_kg(plasticStock.getTotal_kg());
        repo.save(plasticStock1);
        return new ResponseEntity<>(plasticStock1,HttpStatus.OK);
    }

}
