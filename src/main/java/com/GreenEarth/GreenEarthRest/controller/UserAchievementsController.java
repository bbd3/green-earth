package com.GreenEarth.GreenEarthRest.controller;

import com.GreenEarth.GreenEarthRest.Exception.ResourceNotFoundException;
import com.GreenEarth.GreenEarthRest.model.Admin;
import com.GreenEarth.GreenEarthRest.model.User;
import com.GreenEarth.GreenEarthRest.model.UserAchievements;
import com.GreenEarth.GreenEarthRest.repo.UserAchievementsRepo;
import com.GreenEarth.GreenEarthRest.repo.UserRepo;
import com.GreenEarth.GreenEarthRest.service.UserAchievementsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
public class UserAchievementsController {

    @Autowired
    private UserAchievementsService  achievementsService;


    @PostMapping("/user-achievements")
    public ResponseEntity<UserAchievements> insertUserAchievement(@RequestBody UserAchievements userAchievements){
        achievementsService.addAchievements(userAchievements);
        return new ResponseEntity<>(userAchievements, HttpStatus.CREATED);
    }

    @GetMapping("/user-achievements")
    public ResponseEntity<List<UserAchievements>> getAllUserAchievements()
    {
        List<UserAchievements> userAchievements =achievementsService.getAllUserAchievements();


        if(userAchievements.isEmpty())
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        return new ResponseEntity<>(userAchievements,HttpStatus.OK);
    }

    @GetMapping("/user-achievements/{UserAchievementsId}")
    public ResponseEntity<UserAchievements> getUserAchievementsById(@PathVariable Integer UserAchievementsId){
        UserAchievements userAchievements=achievementsService.getUserAchievementsById(UserAchievementsId);


        return new ResponseEntity<>(userAchievements,HttpStatus.OK);

    }
    @GetMapping("/user-achievements/providers-achievements/{BadgeId}")
    public ResponseEntity<List<UserAchievements>> getUserAchievementsByBadgeId(@PathVariable Integer BadgeId){
        List<UserAchievements> userAchievements =achievementsService.getUserAchievementsByBadgeID(BadgeId);


        if(userAchievements.isEmpty())
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        return new ResponseEntity<>(userAchievements,HttpStatus.OK);
    }
    @GetMapping("/user-achievements/userId/{UserId}")
    public ResponseEntity<List<UserAchievements>> getUserAchievementsByUserId(@PathVariable Integer UserId){
        List<UserAchievements> userAchievements =achievementsService.getUserAchievementsByUserID(UserId);


        if(userAchievements.isEmpty())
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        return new ResponseEntity<>(userAchievements,HttpStatus.OK);
    }

    @DeleteMapping("/user-achievements/{UserAchievementsId}")
    public ResponseEntity<UserAchievements> deleteUserAchievementsById(@PathVariable Integer UserAchievementsId){
        achievementsService.deleteAchievement(UserAchievementsId);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
    @PutMapping("/user-achievements/{UserAchievementsId}")
    public ResponseEntity<Object> updateUserById(@RequestBody UserAchievements userAchievements, @PathVariable Integer UserAchievementsId){
        UserAchievements userAchievements1=achievementsService.getUserAchievementsById(UserAchievementsId);


        userAchievements1.setBadges(userAchievements.getBadges());
        userAchievements1.setUser(userAchievements.getUser());
        userAchievements1.setAchievement_date(userAchievements.getAchievement_date());

        achievementsService.editAchievement(userAchievements1);
        return new ResponseEntity<>(userAchievements1,HttpStatus.OK);
    }

//    @GetMapping("/user-achievements")
//    public ResponseEntity<List<UserAchievements>> getUserAchievementsByBadgeId(Integer badgeId)
//    {
//        List<UserAchievements> userAchievements =achievementsService.getUserAchievementsByBadgeID(badgeId);
//
//
//        if(userAchievements.isEmpty())
//            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
//        return new ResponseEntity<>(userAchievements,HttpStatus.OK);
//    }
}
