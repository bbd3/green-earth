package com.GreenEarth.GreenEarthRest.controller;

import com.GreenEarth.GreenEarthRest.Exception.ResourceNotFoundException;
import com.GreenEarth.GreenEarthRest.model.Badges;
import com.GreenEarth.GreenEarthRest.model.PlasticType;
import com.GreenEarth.GreenEarthRest.repo.PlasticTypeRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
public class PlasticTypeController {
    @Autowired
    PlasticTypeRepo plasticTypeRepo;

    @PostMapping("/plastic-types")
    public ResponseEntity<PlasticType> insertplasticType(@RequestBody PlasticType plasticType){
        plasticTypeRepo.save(plasticType);
        return new ResponseEntity<>(plasticType, HttpStatus.CREATED);
    }

    @GetMapping("/plastic-types")
    public ResponseEntity<List<PlasticType>> getAllPlasticTypes()
    {
        List<PlasticType> plasticTypes =new ArrayList<PlasticType>();
        plasticTypes=plasticTypeRepo.findAll();

        if(plasticTypes.isEmpty())
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        return new ResponseEntity<>(plasticTypes,HttpStatus.OK);
    }
    @GetMapping("/plastic-types/{PlasticId}")
    public ResponseEntity<PlasticType> getPlasticTypeById(@PathVariable("PlasticId") Integer PlasticId)
    {
        PlasticType plasticType=plasticTypeRepo.findById(PlasticId)
                .orElseThrow(()->new ResourceNotFoundException("Not found Plastic Type with id = " + PlasticId));

        return new ResponseEntity<>(plasticType,HttpStatus.OK);

    }
    @DeleteMapping("/plastic-types/{PlasticId}")
    public ResponseEntity<HttpStatus> deletePlasticTypeById(@PathVariable Integer PlasticId){
        plasticTypeRepo.deleteById(PlasticId);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
    @PutMapping("/plastic-types/{PlasticId}")
    public ResponseEntity<Object> updatePlasticTypeById(@RequestBody PlasticType plasticType, @PathVariable("PlasticId") Integer PlasticId)
    {
        PlasticType plasticType1= plasticTypeRepo.findById(PlasticId)
                .orElseThrow(()-> new ResourceNotFoundException("Not found Plastic Type with id = "+PlasticId ));
        plasticType1.setPlastic_type(plasticType.getPlastic_type());
        plasticType1.setPrice_per_kg(plasticType.getPrice_per_kg());
        plasticType1.setDescription(plasticType.getDescription());

        plasticTypeRepo.save(plasticType1);
        return new ResponseEntity<>(plasticType1,HttpStatus.OK);
    }

}
