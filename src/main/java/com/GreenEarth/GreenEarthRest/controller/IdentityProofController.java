package com.GreenEarth.GreenEarthRest.controller;
import com.GreenEarth.GreenEarthRest.Exception.ResourceNotFoundException;
import com.GreenEarth.GreenEarthRest.model.Badges;
import com.GreenEarth.GreenEarthRest.model.IdentityProof;
import com.GreenEarth.GreenEarthRest.repo.IdentityProofRepo;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
public class IdentityProofController {


    public final IdentityProofRepo identityProofRepo;

    public IdentityProofController(IdentityProofRepo repo) {
        this.identityProofRepo = repo;
    }

    @PostMapping("/identity-proofs")
    public ResponseEntity<IdentityProof> insertidentityProof(@RequestBody IdentityProof identityProof){
        identityProofRepo.save(identityProof);
        return new ResponseEntity<>(identityProof, HttpStatus.CREATED);
    }

    @GetMapping("/identity-proofs")
    public ResponseEntity<List<IdentityProof>> getAllIdentityProof()
    {
        List<IdentityProof> identityProofs =new ArrayList<IdentityProof>();
        identityProofs=identityProofRepo.findAll();
        if(identityProofs.isEmpty())
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        return new ResponseEntity<>(identityProofs,HttpStatus.OK);
    }
    @GetMapping("/identity-proofs/{ProofId}")
    public ResponseEntity<IdentityProof> getIdentityProofById(@PathVariable("ProofId") Integer ProofId)
    {
        IdentityProof identityProof=identityProofRepo.findById(ProofId)
                .orElseThrow(()->new ResourceNotFoundException("Not found Identity Proof with id = " + ProofId));

        return new ResponseEntity<>(identityProof,HttpStatus.OK);

    }
    @DeleteMapping("/identity-proofs/{ProofId}")
    public ResponseEntity<HttpStatus> deleteIdentityProofById(@PathVariable Integer ProofId){
        identityProofRepo.deleteById(ProofId);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
    @PutMapping("/identity-proofs/{ProofId}")
    public ResponseEntity<Object> updateIdentityProofById(@RequestBody IdentityProof identityProof, @PathVariable("ProofId") Integer ProofId)
    {
        IdentityProof identityProof1= identityProofRepo.findById(ProofId)
                .orElseThrow(()-> new ResourceNotFoundException("Not found Identity Proof with id = "+ProofId ));
        identityProof1.setProof_name(identityProof.getProof_name());
        identityProof1.setSubType(identityProof.getSubType());
        identityProofRepo.save(identityProof1);
        return new ResponseEntity<>(identityProof1,HttpStatus.OK);
    }

}
