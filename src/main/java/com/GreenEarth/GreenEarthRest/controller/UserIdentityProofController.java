package com.GreenEarth.GreenEarthRest.controller;

import com.GreenEarth.GreenEarthRest.Exception.ResourceNotFoundException;
import com.GreenEarth.GreenEarthRest.model.Admin;
import com.GreenEarth.GreenEarthRest.model.IdentityProof;
import com.GreenEarth.GreenEarthRest.model.UserIdentityProof;
import com.GreenEarth.GreenEarthRest.repo.UserIdentityProofRepo;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
public class UserIdentityProofController {

    public final UserIdentityProofRepo userIdentityProofRepo;

    public UserIdentityProofController(UserIdentityProofRepo userIdentityProofRepo) {
        this.userIdentityProofRepo = userIdentityProofRepo;
    }


    @PostMapping("/user-id-proofs")
    public ResponseEntity<UserIdentityProof> insertUserIdentityProof(@RequestBody UserIdentityProof userIdentityProof){
        userIdentityProofRepo.save(userIdentityProof);
        return new ResponseEntity<>(userIdentityProof, HttpStatus.CREATED);
    }

    @GetMapping("/user-id-proofs")
    public ResponseEntity<List<UserIdentityProof>> getAllUserIdentityProofs(){
        List<UserIdentityProof> userIdentityProofs =new ArrayList<UserIdentityProof>();
        userIdentityProofs=userIdentityProofRepo.findAll();

        if(userIdentityProofs.isEmpty())
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        return new ResponseEntity<>(userIdentityProofs,HttpStatus.OK);
    }

    @GetMapping("/user-id-proofs/{UserProofId}")
    public ResponseEntity<UserIdentityProof> getUserIdentityProofById(@PathVariable Integer UserProofId){
        UserIdentityProof userIdentityProof=userIdentityProofRepo.findById(UserProofId)
                .orElseThrow(()->new ResourceNotFoundException("Not found UserProof with id = " + UserProofId));

        return new ResponseEntity<>(userIdentityProof,HttpStatus.OK);

    }
    @DeleteMapping("/user-id-proofs/{UserProofId}")
    public ResponseEntity<HttpStatus> deleteUserIdentityProofById(@PathVariable Integer UserProofId){
        userIdentityProofRepo.deleteById(UserProofId);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
    @PutMapping("/user-id-proofs/{UserProofId}")
    public ResponseEntity<Object> updateUserIdProofById(@RequestBody UserIdentityProof userIdentityProof, @PathVariable Integer UserProofId){
        UserIdentityProof userIdentityProof1=userIdentityProofRepo.findById(UserProofId)
                .orElseThrow(()-> new ResourceNotFoundException("Not found Admin with id = "+UserProofId ));

        userIdentityProof1.setUser(userIdentityProof.getUser());
        userIdentityProof1.setUser_proof_id(userIdentityProof.getUser_proof_id());
        userIdentityProof1.setProof_img(userIdentityProof.getProof_img());

        userIdentityProofRepo.save(userIdentityProof1);
        return new ResponseEntity<>(userIdentityProof1,HttpStatus.OK);
    }

}
