package com.GreenEarth.GreenEarthRest.controller;

import com.GreenEarth.GreenEarthRest.Exception.ResourceNotFoundException;
import com.GreenEarth.GreenEarthRest.model.Admin;
import com.GreenEarth.GreenEarthRest.repo.AdminRepo;
import com.GreenEarth.GreenEarthRest.service.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
public class AdminController {


    @Autowired
    private AdminService adminService;


    public AdminController(AdminService adminService) {
        this.adminService = adminService;
    }

    @PostMapping("/admins")
    public ResponseEntity<Admin> insertAdmin(@RequestBody Admin admin){
        adminService.addAdmin(admin);
        return new ResponseEntity<>(admin, HttpStatus.CREATED);
    }

    @GetMapping("/admins")
    public ResponseEntity<List<Admin>> getAllAdmin()
    {
        List<Admin> admins =adminService.getAllAdmin();

        if(admins.isEmpty())
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        return new ResponseEntity<>(admins,HttpStatus.OK);
    }

    @GetMapping("/admins/{adminId}")
    public ResponseEntity<Admin> getAdminById(@PathVariable("adminId") Integer adminId)
    {
        Admin admin=adminService.getAdminById(adminId);


        return new ResponseEntity<>(admin,HttpStatus.OK);

    }
    @DeleteMapping("/admins/{AdminId}")
    public ResponseEntity<HttpStatus> deleteAdminById(@PathVariable Integer AdminId){
        adminService.deleteAdmin(AdminId);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
    @PutMapping("/admins/{AdminId}")
    public ResponseEntity<Object> updateAdminById(@RequestBody Admin admin, @PathVariable("AdminId") Integer AdminId)
    {
        Admin admin1=adminService.getAdminById(AdminId);


        admin1.setAdmin_name(admin.getAdmin_name());
        admin1.setContact_no(admin.getContact_no());
        admin1.setEmailid(admin.getEmailid());
        admin1.setPassword(admin.getPassword());
        adminService.editAdmin(admin1);
        return new ResponseEntity<>(admin1,HttpStatus.OK);
    }

}

