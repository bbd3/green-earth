package com.GreenEarth.GreenEarthRest.controller;

import com.GreenEarth.GreenEarthRest.model.PlasticDonate;
import com.GreenEarth.GreenEarthRest.service.PlasticDonationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class PlasticDonateController {
    @Autowired
    private PlasticDonationService donationsSerives;

    public PlasticDonateController(PlasticDonationService donationsSerives) {
        this.donationsSerives = donationsSerives;
    }

    @PostMapping("/plastic-donates")
    public ResponseEntity<PlasticDonate> insertPlasticDonate(@RequestBody PlasticDonate plasticDonate) {
        System.out.println("in Controller");
        System.out.println("naksdjh"+plasticDonate.getCurrent_address());

        donationsSerives.addplasticDonations(plasticDonate);
        return new ResponseEntity<>(plasticDonate, HttpStatus.CREATED);
    }

    @GetMapping("/plastic-donates")
    public ResponseEntity<List<PlasticDonate>> getAllPlasticDonate() {
        List<PlasticDonate> plasticDonates = donationsSerives.getAllPlasticDonations();
        if (plasticDonates.isEmpty())
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        return new ResponseEntity<>(plasticDonates, HttpStatus.OK);
    }

    @GetMapping("/plastic-donates/{plasticDonateId}")
    public ResponseEntity<PlasticDonate> getPlasticDonateById(@PathVariable Integer plasticDonateId) {
        PlasticDonate plasticDonate = donationsSerives.getPlasticDonationsIdById(plasticDonateId);

        return new ResponseEntity<>(plasticDonate, HttpStatus.OK);

    }

    @DeleteMapping("/plastic-donates/{plasticDonateId}")
    public ResponseEntity<HttpStatus> deletePlasticDonateById(@PathVariable Integer plasticDonateId) {
        donationsSerives.deletePlasticDonate(plasticDonateId);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);

    }

    @PutMapping("/plastic-donates/{plasticDonateId}")
    public ResponseEntity<Object> updateAdminById(@RequestBody PlasticDonate plasticDonate, @PathVariable Integer plasticDonateId) {
        PlasticDonate plasticDonate1 = donationsSerives.getPlasticDonationsIdById(plasticDonateId);

        plasticDonate1.setUser(plasticDonate.getUser());
        plasticDonate1.setCurrent_address(plasticDonate.getCurrent_address());
        plasticDonate1.setTotal_kg(plasticDonate.getTotal_kg());
        plasticDonate1.setDonate_date(plasticDonate.getDonate_date());
        donationsSerives.editPlasticDonate(plasticDonate1);
        return new ResponseEntity<>(plasticDonate1, HttpStatus.OK);
    }
}
