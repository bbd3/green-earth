package com.GreenEarth.GreenEarthRest.controller;

import com.GreenEarth.GreenEarthRest.Exception.ResourceNotFoundException;
import com.GreenEarth.GreenEarthRest.model.Admin;
import com.GreenEarth.GreenEarthRest.model.PlasticType;
import com.GreenEarth.GreenEarthRest.model.SubType;
import com.GreenEarth.GreenEarthRest.repo.SubTypeRepo;
import com.GreenEarth.GreenEarthRest.service.SubTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;


@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class Sub_TypeController

{

    @Autowired
    private SubTypeService subTypeService;

    public Sub_TypeController(SubTypeService subTypeService) {
        this.subTypeService = subTypeService;
    }

    @PostMapping("/sub-types")
    public ResponseEntity<SubType> insertSubType(@RequestBody SubType subType){
        subTypeService.addSubType(subType);
        return new ResponseEntity<>(subType, HttpStatus.CREATED);
    }



    @GetMapping("/sub-types")
    public ResponseEntity<List<SubType>> getAllSubTypes()
    {

        List<SubType> subTypes =subTypeService.getAllSubType();


        if(subTypes.isEmpty())
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        return new ResponseEntity<>(subTypes,HttpStatus.OK);
    }

    @GetMapping("/sub-types/{SubTypeId}")
    public ResponseEntity<SubType> getSubTypeById(@PathVariable Integer SubTypeId){
        SubType subType=subTypeService.getSubTypeById(SubTypeId);


        return new ResponseEntity<>(subType,HttpStatus.OK);

    }

    @DeleteMapping("/sub-types/{SubTypeId}")
    public ResponseEntity<HttpStatus> deleteSubTypeById(@PathVariable Integer SubTypeId){
        subTypeService.deleteSubType(SubTypeId);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @PutMapping("/sub-types/{SubTypeId}")
    public ResponseEntity<Object> updateAdminById(@RequestBody SubType subType, @PathVariable Integer SubTypeId){
        SubType subType1=subTypeService.getSubTypeById(SubTypeId);


        subType1.setSub_type(subType.getSub_type());

        subTypeService.editSubType(subType1);
        return new ResponseEntity<>(subType1,HttpStatus.OK);
    }
}





