package com.GreenEarth.GreenEarthRest.controller;

import com.GreenEarth.GreenEarthRest.Exception.ResourceNotFoundException;
import com.GreenEarth.GreenEarthRest.model.User;
import com.GreenEarth.GreenEarthRest.repo.UserRepo;
import com.GreenEarth.GreenEarthRest.service.GrrenEarthUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class UserController
{
    @Autowired
    private GrrenEarthUserService userservice;

    public UserController(GrrenEarthUserService userservice) {
        this.userservice = userservice;
    }

    @PostMapping("/users")
    public ResponseEntity<User> insertUser(@RequestBody User user){
        userservice.addUser(user);
        return new ResponseEntity<>(user, HttpStatus.CREATED);
    }

    @GetMapping("/users")
    public ResponseEntity<List<User>> getAllUsers()
    {
       // System.out.println("in controllers");
        List<User> users =userservice.getAllUsers();
        if(users.isEmpty())
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        return new ResponseEntity<>(users,HttpStatus.OK);
    }

    @GetMapping("/users/{UsersId}")
    public ResponseEntity<User> getUsersById(@PathVariable Integer UserId){
        User user=userservice.getUserById(UserId);


        return new ResponseEntity<>(user,HttpStatus.OK);

    }
    @GetMapping("/users/emailId/{emailId}")
    public ResponseEntity<User> getUsersByEmailId(@PathVariable  String emailId){
        User user=userservice.userbyemailid(emailId);
        return new ResponseEntity<>(user,HttpStatus.OK);

    }
    @DeleteMapping("/users/{UserId}")
    public ResponseEntity<User> deleteUsersById(@PathVariable("UserId") Integer UserId){
        userservice.deleteUser(UserId);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
    @PutMapping("/users/{UserId}")
    public ResponseEntity<Object> updateUserById(@RequestBody User users, @PathVariable Integer UserId){
        User users1=userservice.getUserById(UserId);

        users1.setUserType(users.getUserType());
        users1.setSubType(users.getSubType());
        users1.setUser_full_name(users.getUser_full_name());
        users1.setContact_no(users.getContact_no());
        users1.setEmailid(users.getEmailid());
        users1.setPassword(users.getPassword());
        users1.setAddress(users.getAddress());
        userservice.editUser(users1);
        return new ResponseEntity<>(users1,HttpStatus.OK);
    }
}
