package com.GreenEarth.GreenEarthRest.controller;

//import com.GreenEarth.GreenEarthRest.config.AuthenticationRequest;
//import com.GreenEarth.GreenEarthRest.config.AuthenticationResponse;
//import com.GreenEarth.GreenEarthRest.service.UserService;
//import com.GreenEarth.GreenEarthRest.utility.JWTUtility;
import com.GreenEarth.GreenEarthRest.config.AuthenticationRequest;
import com.GreenEarth.GreenEarthRest.config.AuthenticationResponse;
import com.GreenEarth.GreenEarthRest.service.UserService;
import com.GreenEarth.GreenEarthRest.utility.JWTUtility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import javax.management.remote.JMXAuthenticator;

@RestController
public class HomeController
{

    @Autowired
    private  AuthenticationManager authenticationManager;

    @Autowired
    private UserService userService;

    @Autowired
    private JWTUtility jwtUtility;



    @GetMapping("/")
    public  String Home()
    {
        return  "WelcomeTo green Earth";
    }

    @RequestMapping(value = "/authenticate",method = RequestMethod.POST)
    public ResponseEntity<?> createAuthenticationToken(@RequestBody AuthenticationRequest authenticationRequest)
            throws Exception{
        try
        {
            authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(authenticationRequest.getUsername(),authenticationRequest.getPassword())
            );
        }
        catch (BadCredentialsException e)
        {
            throw  new Exception("Incorrect username or password",e);
        }
        final UserDetails userDetails= userService
                .loadUserByUsername(authenticationRequest.getUsername());

        final  String jwt=jwtUtility.generateToken(userDetails);

        return ResponseEntity.ok(new AuthenticationResponse(jwt));

    }

}
