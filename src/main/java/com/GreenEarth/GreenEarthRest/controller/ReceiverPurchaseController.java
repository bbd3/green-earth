package com.GreenEarth.GreenEarthRest.controller;


import com.GreenEarth.GreenEarthRest.Exception.ResourceNotFoundException;
import com.GreenEarth.GreenEarthRest.model.Admin;
import com.GreenEarth.GreenEarthRest.model.ReceiverPurchase;
import com.GreenEarth.GreenEarthRest.repo.ReceiverPurchaseRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class ReceiverPurchaseController
{

    @Autowired
    private ReceiverPurchaseRepo repo;

    @PostMapping("/receiver-purchases")
    public ResponseEntity<ReceiverPurchase> insertReceiverPurchase(@RequestBody ReceiverPurchase receiverPurchase){
        repo.save(receiverPurchase);
        return new ResponseEntity<>(receiverPurchase, HttpStatus.CREATED);
    }

    @GetMapping("/receiver-purchases")
    public ResponseEntity<List<ReceiverPurchase>> getAllReceiverPurchase()
    {
        List<ReceiverPurchase> receiverPurchases =new ArrayList<ReceiverPurchase>();
        receiverPurchases=repo.findAll();

        if(receiverPurchases.isEmpty())
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        return new ResponseEntity<>(receiverPurchases,HttpStatus.OK);
    }
    @GetMapping("/receiver-purchases/{ReceiverPurchaseId}")
    public ResponseEntity<ReceiverPurchase> getReceiverPurchaseById(@PathVariable Integer ReceiverPurchaseId){
        ReceiverPurchase receiverPurchase=repo.findById(ReceiverPurchaseId)
                .orElseThrow(()->new ResourceNotFoundException("Not found ReceiverPurchase with id = " + ReceiverPurchaseId));

        return new ResponseEntity<>(receiverPurchase,HttpStatus.OK);

    }
    @DeleteMapping("/receiver-purchases/{ReceiverPurchaseId}")
    public ResponseEntity<HttpStatus> deleteReceiverPurchaseById(@PathVariable Integer ReceiverPurchaseId){
        repo.deleteById(ReceiverPurchaseId);
       return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
    @PutMapping("/receiver-purchases/{ReceiverPurchaseId}")
    public ResponseEntity<Object> updateReceiverPurchaseById(@RequestBody ReceiverPurchase receiverPurchase, @PathVariable Integer ReceiverPurchaseId){
        ReceiverPurchase receiverPurchase1=repo.findById(ReceiverPurchaseId)
                .orElseThrow(()-> new ResourceNotFoundException("Not found ReceiverPurchase with id = "+ReceiverPurchaseId ));

        receiverPurchase1.setUser(receiverPurchase.getUser());
        receiverPurchase1.setPlasticType(receiverPurchase.getPlasticType());
        receiverPurchase1.setTotal_kg_purchase(receiverPurchase.getTotal_kg_purchase());
        receiverPurchase1.setTotal_price(receiverPurchase.getTotal_price());
        receiverPurchase1.setPurchase_date(receiverPurchase.getPurchase_date());
        repo.save(receiverPurchase1);
        return new ResponseEntity<>(receiverPurchase1,HttpStatus.OK);
    }
}
