package com.GreenEarth.GreenEarthRest.controller;

import com.GreenEarth.GreenEarthRest.Exception.ResourceNotFoundException;
import com.GreenEarth.GreenEarthRest.model.Address;
import com.GreenEarth.GreenEarthRest.model.Admin;
import com.GreenEarth.GreenEarthRest.repo.AddressRepo;
import com.GreenEarth.GreenEarthRest.service.AddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

@RestController
public class AddressController
{
    @Autowired
    private AddressService addressService;



    @PostMapping("/addresses")
    public ResponseEntity<Address> insertAddress(@RequestBody Address address){
        addressService.addAddress(address);
        return new ResponseEntity<>(address, HttpStatus.CREATED);
    }

    @GetMapping("/addresses")
    public ResponseEntity<List<Address>> getAllAddresses()
    {
        List<Address> addresses =addressService.getAllAddress();

        if(addresses.isEmpty())
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        return new ResponseEntity<>(addresses,HttpStatus.OK);
    }
    @GetMapping("/addresses/{AddressId}")
    public ResponseEntity<Address> getAddressById(@PathVariable("AddressId") Integer AddressId)
    {
        Address address=addressService.getAddressById(AddressId);


        return new ResponseEntity<>(address,HttpStatus.OK);

    }
    @DeleteMapping("/addresses/{AddressId}")
    public ResponseEntity<HttpStatus> deleteAddressById(@PathVariable Integer AddressId){
        addressService.deleteAddress(AddressId);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
    @PutMapping("/addresses/{AddressId}")
    public ResponseEntity<Object> updateAddressById(@RequestBody Address address, @PathVariable("AddressId") Integer AddressId)
    {
        Address address1= addressService.getAddressById(AddressId);

        address1.setAddress_line1(address.getAddress_line1());
        address1.setAddress_line2(address.getAddress_line2());
        address1.setCity(address.getCity());
        address1.setPincode(address.getPincode());
        addressService.editAddress(address1);
        return new ResponseEntity<>(address1,HttpStatus.OK);
    }

}
