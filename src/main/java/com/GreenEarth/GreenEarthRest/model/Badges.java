package com.GreenEarth.GreenEarthRest.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "badges")

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Badges {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private  Integer badge_id;

    @Column(name = "badge_name")
    private  String badge_name;

    @ManyToOne
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    @JoinColumn(name = "user_type_id")
    private  UserType BadgesuserType;

    @ManyToOne
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    @JoinColumn(name = "sub_type_id")
    private  SubType subType;

    @Column(name="badge_condition")
    private  float badge_condition;

    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL,mappedBy ="badges")
    private Set<UserAchievements> userAchievements;


}
