package com.GreenEarth.GreenEarthRest.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "identity_proof")

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class IdentityProof {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int proof_id;

    @Column(name = "proof_name")
    private String proof_name;

    @ManyToOne
    @JoinColumn(name = "sub_type_id")
    private  SubType subType;

    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL,mappedBy ="identityProof")
    private Set<UserIdentityProof> userIdentityProofSet;


}
