package com.GreenEarth.GreenEarthRest.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "plastic_type")

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class PlasticType {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int plastic_type_id;
    @Column(name = "plastic_type")
    private String plastic_type;
    @Column(name = "description")
    private String description;
    @Column(name = "price_per_kg")
    private float price_per_kg;

    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL,mappedBy = "plasticType")
    private Set<VolunteersCollection> volunteersCollections;

    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL,mappedBy = "plasticType")
    private Set<ReceiverPurchase> receiverPurchases;

    @JsonIgnore
    @OneToOne(cascade = CascadeType.ALL,mappedBy = "plasticType")
    private  PlasticStock plasticStock;


}
