package com.GreenEarth.GreenEarthRest.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "plastic_stock")

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class PlasticStock
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer stock_id;


    @OneToOne
    @JoinColumn(name = "plastic_type_id")
    private  PlasticType plasticType;

    @Column(name = "total_kg")
    private float total_kg;


}
