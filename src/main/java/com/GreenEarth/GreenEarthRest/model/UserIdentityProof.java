package com.GreenEarth.GreenEarthRest.model;

import lombok.*;
import org.hibernate.type.descriptor.sql.VarbinaryTypeDescriptor;

import javax.persistence.*;

@Entity
@Table(name = "user_id_proof")

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class UserIdentityProof {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private  int user_proof_id;


    @ManyToOne
    @JoinColumn(name = "user_id")
    private  User user;

    @ManyToOne
    @JoinColumn(name = "proof_id")
    private IdentityProof identityProof;

    private Byte[] proof_img;


}
