package com.GreenEarth.GreenEarthRest.model;

import lombok.*;

import javax.persistence.*;
import javax.xml.crypto.Data;
import java.math.BigDecimal;
import java.util.Date;
import java.util.zip.DataFormatException;

@Entity
@Table(name = "receiver_purchase")

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class ReceiverPurchase
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer purchase_id;


    @ManyToOne
    @JoinColumn(name = "user_id")
    private  User user;


    @ManyToOne
    @JoinColumn(name = "plastic_type_id")
    private  PlasticType plasticType;

    @Column(name = "total_kg_purchase")
    private  Float total_kg_purchase;

    @Column(name = "total_price")
    private BigDecimal total_price;

    @Column(name = "purchase_date")
    private Date purchase_date;




}
