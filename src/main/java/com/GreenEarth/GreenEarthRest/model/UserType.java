package com.GreenEarth.GreenEarthRest.model;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Entity
@Table (name = "user_type")

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class UserType
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_type_id")
    private  Integer user_type_id;

    @Column(name = "user_type")
    private  String user_type;

    @JsonIgnore
    @OneToMany(mappedBy ="userType",cascade = CascadeType.ALL)
    private List<User> users;

    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL,mappedBy ="BadgesuserType")
    private Set<Badges> badges;


}
