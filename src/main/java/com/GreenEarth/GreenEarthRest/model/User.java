package com.GreenEarth.GreenEarthRest.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "users")

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = false)
public class User
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id")
    private  Integer user_id;

   // @JsonIgnore
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_type_id")
   private  UserType userType;

    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "sub_type_id")
   private  SubType subType;

    @Column(name = "user_full_name")
    private  String user_full_name;

    @Column(name = "contact_no")
    private  String contact_no;

    @Column(name = "email_id")
    private String emailid;

    @Column(name = "password")
    private String password;

    @OneToOne
    @JoinColumn(name = "address_id")
    private  Address address;


    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL,mappedBy = "user")
    private Set<PlasticDonate> plasticDonates;

    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL,mappedBy = "user")
    private Set<VolunteersCollection> volunteersCollections;

    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL,mappedBy = "user")
    private Set<ReceiverPurchase> receiverPurchases;

    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL,mappedBy = "subType")
    private Set<Badges> badges;

    @JsonIgnore
    @OneToMany(mappedBy ="user",cascade = CascadeType.ALL)
    private List<UserAchievements> userAchievements;


}
