package com.GreenEarth.GreenEarthRest.model;
import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "admin")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Admin {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="admin_id")
    private Integer admin_id;
    @Column(name="admin_name")
    private String admin_name;
    @Column(name="password")
    private String password;
    @Column(name="email_id")
    private String emailid;
    @Column(name="contact_no")
    private String contact_no;


}
