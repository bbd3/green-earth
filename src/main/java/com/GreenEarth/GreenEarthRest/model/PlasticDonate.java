package com.GreenEarth.GreenEarthRest.model;

import com.GreenEarth.GreenEarthRest.service.JsonToPointDeserializer;
import com.GreenEarth.GreenEarthRest.service.PointToJsonSerializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.vividsolutions.jts.geom.Point;
import lombok.*;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "plastic_donate")

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class PlasticDonate
{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer donate_id;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @Column(name = "current_address")
    @JsonSerialize(using = PointToJsonSerializer.class)
    @JsonDeserialize(using = JsonToPointDeserializer.class)
    public Point current_address;

    @Column (name="current_address_text")
    public String current_address_text;

    @Column(name = "total_kg")
    private  float total_kg;

    @Column(name = "donate_date")
    private Date donate_date;




}
