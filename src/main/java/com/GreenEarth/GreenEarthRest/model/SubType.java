package com.GreenEarth.GreenEarthRest.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString

@Table(name = "sub_type")
public class SubType
{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "sub_type_id")
    private  Integer sub_type_id;

    @Column(name = "sub_type")
    private String sub_type;

    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL,mappedBy = "subType")
    private List<User> users;

    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL,mappedBy = "subType")
    private List<Badges> badges;

    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL,mappedBy = "subType")
    private List<IdentityProof> identityProofSet;


}
