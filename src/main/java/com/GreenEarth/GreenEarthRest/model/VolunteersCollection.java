package com.GreenEarth.GreenEarthRest.model;


import lombok.*;
import org.springframework.dao.DataAccessException;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "volunteers_collection")

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class VolunteersCollection
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer collection_id;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private  User user;


    @ManyToOne
    @JoinColumn(name ="plastic_type_id" )
    private  PlasticType plasticType;

    @Column(name = "total_kg")
    private  Float total_kg;

    @Column(name = "collection_date")
    private Date collection_date;



}


