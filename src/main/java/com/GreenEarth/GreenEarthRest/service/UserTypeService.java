package com.GreenEarth.GreenEarthRest.service;

import com.GreenEarth.GreenEarthRest.Exception.ResourceNotFoundException;
import com.GreenEarth.GreenEarthRest.model.Admin;
import com.GreenEarth.GreenEarthRest.model.UserType;
import com.GreenEarth.GreenEarthRest.repo.AdminRepo;
import com.GreenEarth.GreenEarthRest.repo.UserTypeRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserTypeService
{
    @Autowired
    private UserTypeRepo userTypeRepo;


    public List<UserType> getAllUserType()
    {
        return userTypeRepo.findAll();
    }

    public UserType getUserTypeById(Integer userTypeId)
    {
        return userTypeRepo.findById(userTypeId)
                .orElseThrow(()-> new ResourceNotFoundException("Not found userType with id = "+userTypeId ));
    }

    public void addUserType(UserType userType)
    {
        userTypeRepo.save(userType);
    }


    public UserType  editUserType(UserType userType)
    {
        return userTypeRepo.save(userType);
    }


    public void deleteUserType(Integer userTypeId)
    {
        userTypeRepo.deleteById(userTypeId);
    }
}
