package com.GreenEarth.GreenEarthRest.service;


import com.GreenEarth.GreenEarthRest.Exception.ResourceNotFoundException;
import com.GreenEarth.GreenEarthRest.model.*;
import com.GreenEarth.GreenEarthRest.repo.BadgesRepo;
import com.GreenEarth.GreenEarthRest.repo.SubTypeRepo;
import com.GreenEarth.GreenEarthRest.repo.UserTypeRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class badgesService {

    @Autowired
    private BadgesRepo badgesRepo;

    @Autowired
    private UserTypeRepo userTypeRepo;

    @Autowired
    private SubTypeRepo subTypeRepo;

    public List<Badges> getAllBadges() {
        System.out.println("All Data");
        return badgesRepo.findAll();
    }

    public Badges getBadgesIdById(Integer badgesId)
    {
        return badgesRepo.findById(badgesId)
                .orElseThrow(()-> new ResourceNotFoundException("Not found Badge with id = "+badgesId ));
    }

    public Badges addBadge(Badges badges) {

//        UserType userType = userTypeRepo.findById(badges.getBadgesuserType().getUser_type_id()).orElse(null);
//        if (null == userType) {
//            userType = new UserType();
//        }
//        userType.setUser_type(badges.getBadgesuserType().getUser_type());
//        badges.setBadgesuserType(userType);
//
//
//        SubType subType = subTypeRepo.findById(badges.getSubType().getSub_type_id()).orElse(null);
//        if (null == subType) {
//            subType = new SubType();
//        }
//        subType.setSub_type(badges.getSubType().getSub_type());
//        badges.setSubType(subType);
        return badgesRepo.save(badges);

    }

    public Badges editBadge(Badges badges) {




        return badgesRepo.save(badges);
    }

    public void deleteBadge(Integer badgeId) {


        badgesRepo.deleteById(badgeId);
    }
}
