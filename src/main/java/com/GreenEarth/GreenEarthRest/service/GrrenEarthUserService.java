package com.GreenEarth.GreenEarthRest.service;

import com.GreenEarth.GreenEarthRest.Exception.ResourceNotFoundException;
import com.GreenEarth.GreenEarthRest.model.Address;
import com.GreenEarth.GreenEarthRest.model.SubType;
import com.GreenEarth.GreenEarthRest.model.User;
import com.GreenEarth.GreenEarthRest.model.UserType;
import com.GreenEarth.GreenEarthRest.repo.AddressRepo;
import com.GreenEarth.GreenEarthRest.repo.SubTypeRepo;
import com.GreenEarth.GreenEarthRest.repo.UserRepo;
import com.GreenEarth.GreenEarthRest.repo.UserTypeRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.awt.desktop.UserSessionEvent;
import java.util.List;

@Service
public class GrrenEarthUserService
{
    @Autowired
   private UserRepo userRepo;

    @Autowired
   private UserTypeRepo userTypeRepo;

    @Autowired
   private SubTypeRepo subTypeRepo;

    @Autowired
    private AddressRepo addressRepo;

    public List<User> getAllUsers() {
            System.out.println("All Data");
        return userRepo.findAll();
    }

    public User getUserById(Integer userId)
    {
        return userRepo.findById(userId)
                .orElseThrow(()-> new ResourceNotFoundException("Not found user with id = "+userId ));
    }

    public User addUser(User user) {

//        UserType userType = userTypeRepo.findById(user.getUserType().getUser_type_id()).orElse(null);
//      if (null == userType) {
//          userType = new UserType();
//        }
//        userType.setUser_type(user.getUserType().getUser_type());
//        user.setUserType(userType);
//
//
//        SubType subType = subTypeRepo.findById(user.getSubType().getSub_type_id()).orElse(null);
//        if (null == subType) {
//            subType = new SubType();
//        }
//        subType.setSub_type(user.getSubType().getSub_type());
//        user.setSubType(subType);

        Address address=new Address();
        address.setAddress_line1(user.getAddress().getAddress_line1());
        address.setAddress_line2(user.getAddress().getAddress_line2());
        address.setCity(user.getAddress().getCity());
        address.setPincode(user.getAddress().getPincode());

        addressRepo.save(address);
        user.setAddress(address);

        return userRepo.save(user);

    }

    public User editUser(User user) {

        Address address=new Address();
        address.setAddress_line1(user.getAddress().getAddress_line1());
        address.setAddress_line2(user.getAddress().getAddress_line2());
        address.setCity(user.getAddress().getCity());
        address.setPincode(user.getAddress().getPincode());

        addressRepo.save(address);
        user.setAddress(address);
        return userRepo.save(user);
    }

    public void deleteUser(Integer userId) {


        userRepo.deleteById(userId);
    }
    
    public  User userbyemailid(String email)
    {
        User res = null;
       List<User> users =getAllUsers();
        for (User u:users) {
            System.out.println(" emailid  " + email);
            System.out.println("user emailid" + u.getEmailid());
            if(u.getEmailid().equals(email)) {
                res = u;
            }

        }
        return  res;
    }

}
