package com.GreenEarth.GreenEarthRest.service;

import com.GreenEarth.GreenEarthRest.model.Admin;
import com.GreenEarth.GreenEarthRest.repo.AdminRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Optional;

@Service
public class UserService implements UserDetailsService
{

    @Autowired
    private AdminRepo adminRepo;

    @Autowired
    private PasswordEncoder bcryptEncoder;

    @Override
    public UserDetails loadUserByUsername(String username)
            throws UsernameNotFoundException {

            // return new User("admin","admin",new ArrayList<>());


           Admin admin=adminRepo.findByEmailid(username);


            // System.out.println(admin);
            if (admin == null) {
                throw new UsernameNotFoundException("User not found with username: " + username);
            }

            return new org.springframework.security.core.userdetails.User(admin.getEmailid(),bcryptEncoder.encode(admin.getPassword()),
                   new ArrayList<>());

        }


    public AdminRepo save(Admin admin) {
        Admin admin1 = new Admin();

        admin1.setAdmin_id(admin.getAdmin_id());
        admin1.setAdmin_name(admin.getAdmin_name());
        admin1.setContact_no(admin.getContact_no());
        admin1.setEmailid(admin.getEmailid());
        admin1.setPassword(bcryptEncoder.encode(admin.getPassword()));
        return (AdminRepo) adminRepo.save(admin1);
    }

}
