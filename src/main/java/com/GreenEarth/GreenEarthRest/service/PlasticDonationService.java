package com.GreenEarth.GreenEarthRest.service;

import com.GreenEarth.GreenEarthRest.Exception.ResourceNotFoundException;
import com.GreenEarth.GreenEarthRest.model.PlasticDonate;
import com.GreenEarth.GreenEarthRest.repo.PlasticDonateRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PlasticDonationService {
    @Autowired
    private PlasticDonateRepo donateRepo;



    public List<PlasticDonate> getAllPlasticDonations() {

        return donateRepo.findAll();
    }

    public PlasticDonate getPlasticDonationsIdById(Integer plasticDonationsId)
    {
        return donateRepo.findById(plasticDonationsId)
                .orElseThrow(()-> new ResourceNotFoundException("Not found plasticDonations with id = "+plasticDonationsId ));
    }

    public PlasticDonate addplasticDonations(PlasticDonate plasticDonate) {
//      PlasticDonate p =new PlasticDonate();
//      p.setUser(plasticDonate.getUser());
//      p.setTotal_kg(plasticDonate.getTotal_kg());
//      p.setDonate_date(plasticDonate.getDonate_date());
//        GeometryFactory g = new GeometryFactory();
//        Coordinate coord = new Coordinate(plasticDonate.current_address.getX(),plasticDonate.current_address.getY());
//        Point point = g.createPoint(coord);
//        p.setCurrent_address(point);

//        GeometryFactory gf = new GeometryFactory(new PrecisionModel(), 4326);
//        Point location1;
//        location1 = gf.createPoint(new Coordinate(0, 0, 4384));
//        location1.setSRID(4326);
//        se.setLocation(location1);
//        System.out.println("LOCATION 1 SRID: " + se.getLocation().getSRID());
//        System.out.println("geometry srid: " + gf.getSRID());
//        System.out.println("in service");
//        System.out.println(plasticDonate);
        System.out.println(plasticDonate.getCurrent_address());

        return donateRepo.save(plasticDonate);

    }

    public PlasticDonate editPlasticDonate(PlasticDonate plasticDonate) {

        return donateRepo.save(plasticDonate);
    }

    public void deletePlasticDonate(Integer plasticDonateId) {
        donateRepo.deleteById(plasticDonateId);
    }
}
