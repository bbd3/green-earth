package com.GreenEarth.GreenEarthRest.service;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.vividsolutions.jts.geom.Point;

import java.io.IOException;

public class PointToJsonSerializer extends JsonSerializer<Point> {
//
//    @Override
//    public void serialize(Point value, JsonGenerator jgen,
//                          SerializerProvider provider) throws IOException,
//            JsonProcessingException {
//
////        private final static GeometryFactory geometryFactory = new GeometryFactory(new PrecisionModel(), 4326);
//        String jsonValue = "null";
//        try
//        {
//            if(value != null) {
//                double lat = value.getY();
//                double lon = value.getX();
//                jsonValue = String.format("POINT (%s %s)", lat, lon);
//                //GeometryFactory.createPointFromInternalCoord(lat,lon)
//            }
//        }
//        catch(Exception e) {}
//
//        jgen.writeString(jsonValue);
//    }

    @Override
    public void serialize(com.vividsolutions.jts.geom.Point value, JsonGenerator gen, SerializerProvider provider) throws IOException {
        gen.writeStartObject();

        gen.writeNumberField("lat", value.getY());
        gen.writeNumberField("lon", value.getX());
        gen.writeEndObject();
    }
}
