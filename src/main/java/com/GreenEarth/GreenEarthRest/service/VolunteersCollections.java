package com.GreenEarth.GreenEarthRest.service;

import com.GreenEarth.GreenEarthRest.Exception.ResourceNotFoundException;
import com.GreenEarth.GreenEarthRest.model.VolunteersCollection;
import com.GreenEarth.GreenEarthRest.repo.VolunteersCollectionRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class VolunteersCollections {


    @Autowired
    private VolunteersCollectionRepo collectionRepo;

    public List<VolunteersCollection> getAllVolunteersCollection() {
        // System.out.println("All Data");
        return collectionRepo.findAll();
    }

    public VolunteersCollection getVolunteersCollectionById(Integer volunteersCollectionId)
    {
        return collectionRepo.findById(volunteersCollectionId)
                .orElseThrow(()-> new ResourceNotFoundException("Not found volunteersCollection volunteersCollectionId with id = "+volunteersCollectionId ));
    }

    public VolunteersCollection addVolunteersCollection(VolunteersCollection volunteersCollection) {

        return collectionRepo.save(volunteersCollection);

    }

    public VolunteersCollection editVolunteersCollection(VolunteersCollection volunteersCollection) {


        return collectionRepo.save(volunteersCollection);
    }

    public void deleteVolunteersCollection(Integer volunteersCollectionId) {


        collectionRepo.deleteById(volunteersCollectionId);
    }


    public List<VolunteersCollection> getVolunteersCollectionByUserID(Integer UserId)
    {
        ArrayList<VolunteersCollection> res=new ArrayList<VolunteersCollection>();
        List<VolunteersCollection> VolunteersCollection= collectionRepo.findAll();
        for(VolunteersCollection a:VolunteersCollection)
        {
            if(a.getUser().getUser_id() == UserId )
            {
                res.add(a);
            }
        }
        return res ;
    }
}
