package com.GreenEarth.GreenEarthRest.service;

import com.GreenEarth.GreenEarthRest.Exception.ResourceNotFoundException;
import com.GreenEarth.GreenEarthRest.model.*;
import com.GreenEarth.GreenEarthRest.repo.BadgesRepo;
import com.GreenEarth.GreenEarthRest.repo.UserAchievementsRepo;
import com.GreenEarth.GreenEarthRest.repo.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserAchievementsService {

    @Autowired
    private UserAchievementsRepo achievementsRepo;

    @Autowired
    private UserRepo userRepo;

    @Autowired
    private BadgesRepo badgesRepo;


    public List<UserAchievements> getAllUserAchievements() {
       // System.out.println("All Data");
        return achievementsRepo.findAll();
    }

    public UserAchievements getUserAchievementsById(Integer achievementId)
    {
        return achievementsRepo.findById(achievementId)
                .orElseThrow(()-> new ResourceNotFoundException("Not found User achievementId with id = "+achievementId ));
    }

    public UserAchievements addAchievements(UserAchievements achievements) {

//        User user = userRepo.findById(achievements.getUser().getUser_id()).orElse(null);
//        if (null == user) {
//            user = new User();
//        }
//        user.setUserType(achievements.getUser().getUserType());
//        user.setSubType(achievements.getUser().getSubType());
//        user.setUser_full_name(achievements.getUser().getUser_full_name());
//        user.setContact_no(achievements.getUser().getContact_no());
//        user.setEmailid(achievements.getUser().getEmailid());
//        user.setPassword(achievements.getUser().getPassword());
//        user.setAddress(achievements.getUser().getAddress());
//
//        achievements.setUser(user);
//
//
//        Badges badges = badgesRepo.findById(achievements.getBadges().getBadge_id()).orElse(null);
//        if (null == badges) {
//            badges = new Badges();
//        }
//        badges.setBadge_name(achievements.getBadges().getBadge_name());
//        badges.setBadgesuserType(achievements.getBadges().getBadgesuserType());
//        badges.setSubType(achievements.getBadges().getSubType());
//        badges.setBadge_condition(achievements.getBadges().getBadge_condition());


        return achievementsRepo.save(achievements);

    }

    public UserAchievements editAchievement(UserAchievements achievements) {


        return achievementsRepo.save(achievements);
    }

    public void deleteAchievement(Integer achievementsId) {


        achievementsRepo.deleteById(achievementsId);
    }

    public List<UserAchievements> getUserAchievementsByBadgeID(Integer badgeId)
    {
           ArrayList<UserAchievements> res=new ArrayList<UserAchievements>();
      List<UserAchievements> userAchievementsList= achievementsRepo.findAll();
      for(UserAchievements a:userAchievementsList)
      {
          if(a.getBadges().getBadge_id() == badgeId &&  !(a.getUser().getUserType().getUser_type().equals("Volunteers")))
          {
              res.add(a);
          }
      }
      return res ;
    }
    public List<UserAchievements> getUserAchievementsByUserID(Integer UserId)
    {
        ArrayList<UserAchievements> res=new ArrayList<UserAchievements>();
        List<UserAchievements> userAchievementsList= achievementsRepo.findAll();
        for(UserAchievements a:userAchievementsList)
        {
            if(a.getUser().getUser_id() == UserId )
            {
                res.add(a);
            }
        }
        return res ;
    }

}
