package com.GreenEarth.GreenEarthRest.service;


import com.GreenEarth.GreenEarthRest.Exception.ResourceNotFoundException;
import com.GreenEarth.GreenEarthRest.model.Admin;
import com.GreenEarth.GreenEarthRest.repo.AddressRepo;
import com.GreenEarth.GreenEarthRest.repo.AdminRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AdminService {

    @Autowired
   private AdminRepo adminRepo;


    public List<Admin> getAllAdmin()
    {
        return adminRepo.findAll();
    }

    public Admin getAdminById(Integer adminID)
    {
        return adminRepo.findById(adminID)
                .orElseThrow(()-> new ResourceNotFoundException("Not found Admin with id = "+adminID ));
    }

    public void addAdmin(Admin admin)
    {
        adminRepo.save(admin);
    }


    public Admin  editAdmin(Admin admin)
    {
        return adminRepo.save(admin);
    }


    public void deleteAdmin(Integer adminId)
    {
        adminRepo.deleteById(adminId);
    }


}
