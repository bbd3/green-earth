package com.GreenEarth.GreenEarthRest.service;

import com.GreenEarth.GreenEarthRest.Exception.ResourceNotFoundException;
import com.GreenEarth.GreenEarthRest.model.SubType;
import com.GreenEarth.GreenEarthRest.model.UserType;
import com.GreenEarth.GreenEarthRest.repo.SubTypeRepo;
import com.GreenEarth.GreenEarthRest.repo.UserTypeRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SubTypeService
{
    @Autowired
    private SubTypeRepo subTypeRepo;


    public List<SubType> getAllSubType()
    {
        return subTypeRepo.findAll();
    }

    public SubType getSubTypeById(Integer subTypeId)
    {
        return subTypeRepo.findById(subTypeId)
                .orElseThrow(()-> new ResourceNotFoundException("Not found SubType with id = "+subTypeId ));
    }

    public void addSubType(SubType subType)
    {
        subTypeRepo.save(subType);
    }


    public SubType  editSubType(SubType subType)
    {
        return subTypeRepo.save(subType);
    }


    public void deleteSubType(Integer subTypeId)
    {
        subTypeRepo.deleteById(subTypeId);
    }
}
