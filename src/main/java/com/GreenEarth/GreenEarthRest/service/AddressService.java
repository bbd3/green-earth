package com.GreenEarth.GreenEarthRest.service;

import com.GreenEarth.GreenEarthRest.Exception.ResourceNotFoundException;
import com.GreenEarth.GreenEarthRest.model.Address;
import com.GreenEarth.GreenEarthRest.model.Admin;
import com.GreenEarth.GreenEarthRest.repo.AddressRepo;
import com.GreenEarth.GreenEarthRest.repo.AdminRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AddressService
{
    @Autowired
    private AddressRepo addressRepo;


    public List<Address> getAllAddress()
    {
        return addressRepo.findAll();
    }

    public Address getAddressById(Integer addressId)
    {
        return addressRepo.findById(addressId)
                .orElseThrow(()-> new ResourceNotFoundException("Not found Admin with id = "+addressId ));
    }

    public void addAddress(Address address)
    {
        addressRepo.save(address);
    }


    public Address  editAddress(Address address)
    {
        return addressRepo.save(address);
    }


    public void deleteAddress(Integer addressId)
    {
        addressRepo.deleteById(addressId);
    }


}
