package com.GreenEarth.GreenEarthRest;

import com.GreenEarth.GreenEarthRest.repo.AdminRepo;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication(exclude =  { SecurityAutoConfiguration.class })

public class GreenEarthRestApplication {

	public static void main(String[] args) {
		SpringApplication.run(GreenEarthRestApplication.class, args);
	}

}
