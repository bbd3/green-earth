package com.GreenEarth.GreenEarthRest.config;


import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class AuthenticationResponse
{
    private  final  String jwt;
}
