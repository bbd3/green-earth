package com.GreenEarth.GreenEarthRest.repo;

import com.GreenEarth.GreenEarthRest.model.ReceiverPurchase;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ReceiverPurchaseRepo extends JpaRepository<ReceiverPurchase,Integer> {
}
