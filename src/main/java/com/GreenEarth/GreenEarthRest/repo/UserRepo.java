package com.GreenEarth.GreenEarthRest.repo;

import com.GreenEarth.GreenEarthRest.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;


public interface UserRepo extends JpaRepository<User,Integer> {


}
