package com.GreenEarth.GreenEarthRest.repo;

import com.GreenEarth.GreenEarthRest.model.Admin;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AdminRepo extends JpaRepository<Admin, Integer> {

   Admin findByEmailid(String username);

}



