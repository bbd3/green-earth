package com.GreenEarth.GreenEarthRest.repo;

import com.GreenEarth.GreenEarthRest.model.PlasticType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PlasticTypeRepo extends JpaRepository<PlasticType,Integer>{
}
