package com.GreenEarth.GreenEarthRest.repo;

import com.GreenEarth.GreenEarthRest.model.SubType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SubTypeRepo extends JpaRepository<SubType,Integer>
{
}
