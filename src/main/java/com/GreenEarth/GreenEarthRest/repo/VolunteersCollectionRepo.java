package com.GreenEarth.GreenEarthRest.repo;

import com.GreenEarth.GreenEarthRest.model.VolunteersCollection;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VolunteersCollectionRepo extends JpaRepository<VolunteersCollection,Integer> {
}
