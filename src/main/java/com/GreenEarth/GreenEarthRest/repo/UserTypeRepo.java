package com.GreenEarth.GreenEarthRest.repo;

import com.GreenEarth.GreenEarthRest.model.UserType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserTypeRepo extends  JpaRepository<UserType,Integer>
{
}
