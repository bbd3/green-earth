package com.GreenEarth.GreenEarthRest.repo;

import com.GreenEarth.GreenEarthRest.model.Badges;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BadgesRepo extends JpaRepository<Badges,Integer> {
}
