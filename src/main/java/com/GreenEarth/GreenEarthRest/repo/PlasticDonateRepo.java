package com.GreenEarth.GreenEarthRest.repo;

import com.GreenEarth.GreenEarthRest.model.PlasticDonate;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PlasticDonateRepo extends JpaRepository<PlasticDonate,Integer> {
}
