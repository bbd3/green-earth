package com.GreenEarth.GreenEarthRest.repo;

import com.GreenEarth.GreenEarthRest.model.UserAchievements;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserAchievementsRepo extends JpaRepository<UserAchievements,Integer> {


}
