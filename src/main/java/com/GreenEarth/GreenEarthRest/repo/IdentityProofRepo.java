package com.GreenEarth.GreenEarthRest.repo;

import com.GreenEarth.GreenEarthRest.model.IdentityProof;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IdentityProofRepo extends JpaRepository<IdentityProof, Integer> {
}
