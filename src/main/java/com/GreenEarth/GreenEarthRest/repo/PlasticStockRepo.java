package com.GreenEarth.GreenEarthRest.repo;

import com.GreenEarth.GreenEarthRest.model.PlasticStock;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PlasticStockRepo extends JpaRepository<PlasticStock,Integer> {
}
