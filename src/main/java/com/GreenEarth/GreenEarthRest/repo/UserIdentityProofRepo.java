package com.GreenEarth.GreenEarthRest.repo;

import com.GreenEarth.GreenEarthRest.model.UserIdentityProof;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserIdentityProofRepo extends JpaRepository<UserIdentityProof,Integer> {
}
